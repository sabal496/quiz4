package com.example.quiz3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    var datalist= mutableListOf<Mymodel>()
      var ensignUrl:String=""
    lateinit var adapter:Adapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       SetUp()
    }

    private fun SetUp(){
        Api.GetRequest("v2/competitions",object : Getjson{
            override fun onResponse(value: String) {
                d("rr",value)

                parsejson(value)

             }

            override fun onFailure(value: String) {

             }

        })



    }

   fun  parsejson(jsonString:String){

       val json=JSONObject(jsonString)
        if(jsoncheck(json,"competitions")){
            d("keke","sdsa")
            var comparray=json.getJSONArray("competitions")
            (0 until comparray.length()).forEach(){
                i->
                var area=comparray.getJSONObject(i).getJSONObject("area")

                    var id=area.getInt("id")
                    var name=area.getString("name")
                    var countryCode=area.getString("countryCode")

                if(jsoncheck(area,"ensignUrl")){
                      ensignUrl=area.getString("ensignUrl")
                }


                  datalist.add(Mymodel(id,name,countryCode,ensignUrl))



            }
            adapter= Adapter(datalist,MainActivity())
            recycle.layoutManager= GridLayoutManager(this,2)
            recycle.adapter=adapter



        }


   }

    fun jsoncheck(jsonobject: JSONObject, key:String):Boolean{

        if(!jsonobject.has(key)) return false
        return true

    }
}
