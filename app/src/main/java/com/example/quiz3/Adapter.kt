package com.example.quiz3

import android.app.Activity
import android.net.Uri
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import kotlinx.android.synthetic.main.cardview.view.*

class Adapter(val mylist:MutableList<Mymodel>,val act:Activity):RecyclerView.Adapter<Adapter.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        return holder(LayoutInflater.from(parent.context).inflate(R.layout.cardview,parent,false))
    }

    override fun getItemCount(): Int {
        return mylist.size
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.onbind()
    }

    inner class holder(itemView:View):RecyclerView.ViewHolder(itemView){
        private  lateinit var model:Mymodel

        fun onbind(){
            model=mylist[adapterPosition]
            itemView.idid.text=model.id.toString()
            itemView.name.text=model.name
            itemView.countryCode.text=model.countryCode
            d("dddd",model.name)
           /* if(model.ensignUrl!=null)
            GlideToVectorYou.justLoadImage(act, Uri.parse(model.ensignUrl), itemView.avatar)*/
          //  notifyDataSetChanged()
        }


    }
}