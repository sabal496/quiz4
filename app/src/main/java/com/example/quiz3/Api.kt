package com.example.quiz3

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

object Api {

    var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://api.football-data.org/")
        .build()

    var service = retrofit.create(Apiservice::class.java)

    fun GetRequest(path :String,callback:Getjson){
        var call=service.request(path)
        call.enqueue(object :Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
          callback.onFailure(t.message.toString())

            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                callback.onResponse(response.body().toString())

            }

        })
    }


    interface Apiservice {
        @GET("{path}")
        fun request(@Path("path") path: String): Call<String>
    }
}